var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var arraySize = 39
var array = create2DArray(arraySize, arraySize, 0, false)
var initSize = 0.018
var frame = 0

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth * 0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < array.length; i++) {
    noFill()
    stroke(255)
    strokeWeight(boardSize * 0.004)
    curveTightness(sin(frame * 0.913))
    beginShape()
    for (var j = 0; j < array[i].length; j++) {
      curveVertex(windowWidth * 0.5 + (i - Math.floor(arraySize * 0.5)) * boardSize * initSize + boardSize * initSize * sin(frame * Math.PI * 2 * array[i][j]) * (j / arraySize) * sin(frame * 1.189), windowHeight * 0.5 + (j - Math.floor(arraySize * 0.5)) * boardSize * initSize * 1.2 + boardSize * initSize * cos(frame * Math.PI * 2 * array[i][j]) * (j / arraySize) * sin(frame * 0.789))
    }
    endShape()
  }
  frame += deltaTime * 0.00025
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = Math.random()
      }
    }
    array[i] = columns
  }
  return array
}
